/*
 * Copyright CINES, 2017 Ce logiciel est un programme informatique servant à créer une interface Web
 * pour valider des formats de fichiers. Ce logiciel est régi par la licence CeCILL-C soumise au
 * droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 * redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 * l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 * à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 * leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 * leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 * sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */
package fr.cines.pac.service.ingest.xml.adapter;

import java.io.File;
import java.util.Locale;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.MethodSorters;

import fr.cines.pac.sip.DocDCType;
import fr.cines.pac.sip.DocMetaType;
import fr.cines.pac.sip.ObjectFactory;
import fr.cines.pac.sip.PacType;
import fr.cines.pac.sip.StringNotNULLtext;

/**
 * The Class LocaleAdapterManualTest.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LocaleAdapterManualTest {

    /** The JUnit rule logging the name. */
    @Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void starting(final Description description) {
            System.out.println("Démarrage du test: " + description.getMethodName());
        }
    };

    /**
     * New value.
     * @param value the value
     * @return the string not NUL ltext
     */
    private StringNotNULLtext newValue(final String value) {
        final StringNotNULLtext result = new StringNotNULLtext();

        result.setValue(value);
        result.setLanguage(new Locale("und", "und"));

        return result;
    }

    /**
     * Test adapter.
     * @throws Exception the exception
     */
    @Test
    public void testAdapter() throws Exception {
        // Ce test echoue sur Jenkins suite à une erreur de 'reflection'.
        final ObjectFactory objectFactory = new ObjectFactory();
        final PacType root = new PacType();
        final DocDCType docDc = new DocDCType();
        final DocMetaType docMeta = new DocMetaType();
        final String notSpecifiedValue = "Non renseigné";

        root.setDocDC(docDc);
        root.setDocMeta(docMeta);

        docDc.getTitle().add(newValue(notSpecifiedValue));
        docDc.getCreator().add(notSpecifiedValue);
        docDc.getSubject().add(newValue(notSpecifiedValue));
        docDc.getDescription().add(newValue(notSpecifiedValue));
        docDc.getPublisher().add("mnhn");
        docDc.setDate(notSpecifiedValue);
        docDc.getType().add(newValue(notSpecifiedValue));
        docDc.getFormat().add(newValue(notSpecifiedValue));
        docDc.getLanguage().add(Locale.FRENCH);
        docDc.getRights().add(newValue(notSpecifiedValue));

        docMeta.setIdentifiantDocProducteur("P00115130");
        docMeta.setServiceVersant("mnhn");
        docMeta.setPlanClassement(newValue("/herbier"));

        // ecriture
        final File file = new File("target/sip.xml");
        final JAXBContext context = JAXBContext.newInstance(PacType.class);
        final Marshaller marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(root, file);
        marshaller.marshal(objectFactory.createPac(root), System.out);

        // lecture
        final Unmarshaller unmarshaller = context.createUnmarshaller();
        final PacType read = (PacType) unmarshaller.unmarshal(file);
        System.out.println("Contenu généré :");
        System.out.println(ToStringBuilder.reflectionToString(read, new RecursiveToStringStyle()));

        // validation
        final Source xmlFile = new StreamSource(file);
        final File schemaFile = new File("src/main/xsd/sip.xsd");
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        final Schema schema = schemaFactory.newSchema(schemaFile);
        final javax.xml.validation.Validator validator = schema.newValidator();
        validator.validate(xmlFile);
        System.out.println(xmlFile.getSystemId() + " is valid");
    }
}
