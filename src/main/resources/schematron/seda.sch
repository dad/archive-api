<?xml version="1.0" encoding="UTF-8"?>

<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

		Titre   : Schematron: règles de gestion SEDA - contrôles métier
		Auteur  : Pascal Dugénie dugenie@cines.fr
		Version : 2.0
		Creation: 03/04/2013
		Modifications: 26/04/2017: règles B3 & C1, ajout règle D1

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<schema
xmlns="http://purl.oclc.org/dsdl/schematron"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:seda="fr:gouv:culture:archivesdefrance:seda:v1.0"
>

<ns
uri="fr:gouv:culture:archivesdefrance:seda:v1.0" prefix="seda"
/>



<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A1 (1/2): Vérification de l'antériorité OldestDate par rapport à LatestDate
	Code ereeur: E0301

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
--> 
<pattern>
<rule context="seda:ContentDescription[(seda:LatestDate) and (seda:OldestDate)]">

<let name="endDate" value="normalize-space(translate(seda:LatestDate, '- ', ''))" />

<let name="startDate" value="normalize-space(translate(seda:OldestDate, '- ', ''))" />

<assert test="number($endDate) >= number($startDate)">
+++
Erreur: la date de fin <value-of select="seda:LatestDate"/>
est antérieure à la date de début <value-of select="seda:OldestDate"/>
+++
</assert>

</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A1 (2/2): Vérification de la déclaration commune de OldestDate & LatestDate
	Code erreur: E0302

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
--> 
<pattern>
<rule context="seda:*/seda:ContentDescription[(seda:LatestDate) or (seda:OldestDate)]">

<let name="nbLD"  value="count(.[(seda:LatestDate)])" />
<let name="nbOD"  value="count(.[(seda:OldestDate)])" />

<assert test="( $nbLD = count(.) ) and ( $nbOD = count(.) )">
+++
Erreur: les valeurs OldestDate et LatestDate doivent toujours être déclarées ensemble.
+++
</assert>

</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A2 (1/2): Vérification que la date de fin soit cohérente à celle du parent
	Code erreur: E0303

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->
<pattern>
<rule context="seda:LatestDate">

<let name="endDate" value="normalize-space(translate(., '- ', ''))" />

<let name="ancestorEndDate" value="normalize-space(translate(parent::seda:ContentDescription/../ancestor::seda:*[seda:ContentDescription/seda:LatestDate][1]/seda:ContentDescription/seda:LatestDate, '- ', ''))" /> 

<assert test="($ancestorEndDate = '') or (($endDate) &lt;= ($ancestorEndDate))">
+++
Erreur: la date de fin <value-of select="."/> est postérieure à la date de fin d'un niveau parent
<value-of select="parent::seda:ContentDescription/../ancestor::seda:*[seda:ContentDescription/seda:LatestDate][1]/seda:ContentDescription/seda:LatestDate"/>
+++
</assert>

</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A2 (2/2): Vérification que la date de début soit cohérente à celle du parent
	Code erreur: E0304

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->
<pattern>
<rule context="seda:OldestDate">
<let name="startDate" value="normalize-space(translate(., '- ', ''))" />
<let name="ancestorStartDate"
value="normalize-space(translate(parent::seda:ContentDescription/../ancestor::seda:*[seda:ContentDescription/seda:OldestDate][1]/seda:ContentDescription/seda:OldestDate, '- ', ''))" />

<assert test="($ancestorStartDate = '') or (($startDate) >= ($ancestorStartDate))">
+++
Erreur: la date de début <value-of select="."/> est antérieure à la date de début d'un niveau parent
<value-of select="parent::seda:ContentDescription/../ancestor::seda:*[seda:ContentDescription/seda:OldestDate][1]/seda:ContentDescription/seda:OldestDate"/>
+++
</assert>

</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A3 (1/6): le type d'objet 'item' doit toujours être au niveau le plus bas dans la hiérarchie
	Code erreur: E0305

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->
<pattern>
<rule context="seda:ArchiveObject[ancestor::seda:*[seda:ContentDescription/seda:DescriptionLevel='item']]">

<let name="level" value="seda:ContentDescription/seda:DescriptionLevel" />
			
<let name="mytest" value="count(.)" />

<assert test=" $mytest = 0 ">
+++
Erreur: lorsque le niveau de description DescriptionLevel=item est renseigné,
la valeur '<value-of select="$level"/>' ne peut pas &#xea;tre indiquée à un niveau inférieur dans cette branche.
+++
</assert>
</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A3 (2/6): types d'objet 'fonds' et 'subfonds'

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->


<pattern>

<rule context="seda:*[ seda:ContentDescription/seda:DescriptionLevel[

(.='fonds') or (.='subfonds')

] ]">

<let name="sel" value="./seda:*[child::seda:ContentDescription/seda:DescriptionLevel[

(.='fonds') or (.='subseries') or (.='subgrp') or (.='class') or (.='collection')

] ]"
/>
<assert test="count($sel) = 0">
+++
Erreur: certaines déclarations de niveaux de hiérarchie des objets sont incohérentes
+++
</assert>
</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A3 (3/6): types d'objet 'series' et 'subseries'

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->
<pattern>

<rule context="seda:*[ seda:ContentDescription/seda:DescriptionLevel[

(.='series') or (.='subseries')

] ]">


<let name="sel" value="./seda:*[child::seda:ContentDescription/seda:DescriptionLevel[

(.='fonds') or (.='subfonds') or (.='series') or (.='recordgrp')  or (.='subgrp') or (.='class') or (.='collection')

] ]" />


<assert test="count($sel) = 0">
+++
Erreur: le type d'objet 'series' ou 'subseries'
ne peut pas contenir une sub-division 'fonds' 'subfonds' 'recordgrp' 'subgrp' 'class' 'collection'
+++
</assert>


</rule>

</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A3 (4/6): type d'objet 'file'

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->
<pattern>
<rule context="seda:*[ seda:ContentDescription/seda:DescriptionLevel[(.='file')] ]">

<let name="sel" 
value="./seda:*[child::seda:ContentDescription/seda:DescriptionLevel[
(.='fonds') or (.='subfonds') or (.='series') or (.='subseries') or (.='recordgrp')  or (.='subgrp') or (.='class') or (.='collection')
] ]"
/>
<assert test="count($sel) = 0">
+++
Erreur: certaines déclarations de niveaux de hiérarchie des objets sont incohérentes
+++
</assert>
</rule>
</pattern>





<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A3 (5/6): types d'objet 'recordgrp' 'subgrp' 'class' 'collection'

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>
<rule context="seda:*[ seda:ContentDescription/seda:DescriptionLevel[
(.='recordgrp')  or (.='subgrp')
] ]"
>

<let name="sel" 
value="./seda:*[child::seda:ContentDescription/seda:DescriptionLevel[
(.='fonds') or (.='subfonds') or (.='series') or (.='subseries') or (.='recordgrp') or (.='class') or (.='collection')
] ]"
/>
<assert test="count($sel) = 0">
+++
Erreur: certaines déclarations de niveaux de hiérarchie des objets sont incohérentes.
(au moins une déclaration fonds, subfonds, series, subseries, recordgrp, class ou collection
est déclarée sous un élément de type recordgrp ou subgrp)
+++
</assert>
</rule>
</pattern>




<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A3 (6/6): types d'objet 'class' 'collection'

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>
<rule context="seda:*[ seda:ContentDescription/seda:DescriptionLevel[
(.='class') or (.='collection')
] ]">

<let name="sel" 
	value="./seda:*[child::seda:ContentDescription/seda:DescriptionLevel[
(.='fonds') or (.='subfonds') or (.='series') or (.='subseries')
] ]"
	/>

<let name="level" value="seda:ContentDescription/seda:DescriptionLevel" />

<assert test="count($sel) = 0">
+++
Erreur: la déclaration de niveau de hiérarchie de type <value-of select="$level"/> est en dessous
d'un niveau de type 'class' ou 'collection'.
+++
</assert>

</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A4: identifiant SV ou SP

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>
<rule context="seda:Archive[ not(seda:TransferringAgencyArchiveIdentifier) and not(seda:OriginatingAgencyArchiveIdentifier) ]" >

<assert test="count(.) = 0" >
+++
Erreur: une des deux MD est manquante: il est nécéssaire de préciser un identifiant
soit 'TransferringAgencyArchiveIdentifier', soit 'OriginatingAgencyArchiveIdentifier'
+++
</assert>

</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A5: language

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>

<rule context="seda:ArchiveObject[seda:ContentDescription/seda:Language]">

<let name="lang" value="seda:ContentDescription/seda:Language" />

<let name="ancestorlang"  value="

ancestor::seda:*[seda:ContentDescription/seda:Language [ .=string($lang) ]]

"/>

<assert test="count($ancestorlang) > 0 ">
+++
Erreur: la langue de l'object (<value-of select="$lang"/>)
est différente de celle d'un objet parent (<value-of select="$ancestorlang"/>)
+++
</assert>
</rule>

</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A6: Plan de classement

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<!--
TODO: mettre la valeur du champ correspondant à FilePlanPosition
-->


<pattern>
<rule context="seda:ArchiveObject[seda:ContentDescription/seda:FilePlanPosition]">

<assert test="count(.) = 0">
+++
Erreur: la métadonnée FilePlanPosition est déclarée à un niveau ArchiveObject.
La déclaration du plan de classement n'est autorisée qu'au niveau Archive.
+++
</assert>

</rule>
</pattern>



<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle A7: Attachment, Présence de filename et de uri

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>
<rule context="seda:Attachment[ (@uri) and (@filename) ]">

<assert test="count(.) = 0">
+++
Erreur: les attributs "filename" et "uri" ne doivent pas être renseignés pour un même Document
+++
</assert>

</rule>
</pattern>



<pattern>

<title>
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

        Règle XX: LatestDate doit être antérieure à la date du jour

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
</title>
<rule context="seda:Archive" >

<let name="ld"
value="seda:ContentDescription/seda:LatestDate"
/>

<let name="jour"
value="format-date(current-date(), '[Y0001]-[M01]-[D01]')"
/>

<let name="ld_num"
value="number(normalize-space(translate($ld,'-','')))"
/>

<let name="jour_num"
value="number(normalize-space(translate($jour,'-','')))"
/>


<assert test="$ld_num &lt;= $jour_num" >

+++
Erreur: la LatestDate = <value-of select="$ld"  /> ne doit pas être postérieure à la date du jour <value-of select="$jour"  />
+++

</assert>

</rule>

</pattern>




<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle B1 (1/2): Appraisal, la DUA doit toujours figurer au niveau Archive

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->
<pattern>

<rule context="seda:Archive[ not(seda:AppraisalRule) ]">

<assert test="( count(.) = 0 )" >
+++
Erreur: la DUA (bloc AppraisalRule) ne figure pas au niveau Archive.
+++
</assert>

</rule>

</pattern>



<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle B1 (2/2): Si la DUA ne figure pas au niveau objet, elle doit pouvoir être héritée d'un niveau supérieur

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>

<rule context="seda:ArchiveObject[ not(seda:AppraisalRule) ]">

<let name="ancestor_appraisal"
value="ancestor::seda:*[seda:AppraisalRule]"
/>

<assert test="( count($ancestor_appraisal) > 0 )" >
+++
Erreur: la DUA (bloc AppraisalRule) ne figure pas sur toutes les branches d'un niveau
et ne peut pas être déduite des niveaux supérieurs.
+++
</assert>

</rule>

</pattern>






<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle B2: Appraisal Code

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>

<rule context="seda:ArchiveObject[ seda:AppraisalRule/seda:Code='conserver' ]">

<let name="ancestor_appraisal"
value="ancestor::seda:*[seda:AppraisalRule/seda:Code='detruire']"
/>

<assert test="( count($ancestor_appraisal) = 0 )" >
+++
Erreur: au moins un objet dont le code de la DUA est 'détruire' contient des objets fils avec une valeur 'conserver'.
+++
</assert>

</rule>

</pattern>




<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle B3: Appraisal StartDate

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>

<!--
<rule context="seda:ArchiveObject[ seda:AppraisalRule/seda:StartDate ]">
-->
<rule context="seda:*[ seda:AppraisalRule/seda:StartDate ]">



<let name="dossierPerso"
value="seda:ContentDescription/seda:Keyword/seda:KeywordContent"
/>

<let name="sd"
value="seda:AppraisalRule/seda:StartDate"
/>

<let name="ld"
value="seda:ContentDescription/seda:LatestDate"
/>

<let name="od"
value="seda:ContentDescription/seda:OldestDate"
/>

<let name="ld_ancestor"
value="parent::*/parent::seda:*/seda:ContentDescription/seda:LatestDate"
/>

<let name="od_ancestor"
value="parent::*/parent::seda:*/seda:ContentDescription/seda:OldestDate"
/>


<assert test='(

(
( number(normalize-space(translate($sd, "-", ""))) &lt;= number(normalize-space(translate($ld         ,"-",""))) ) and
( number(normalize-space(translate($sd, "-", "")))    >= number(normalize-space(translate($od         ,"-",""))) )
)
or
(
( number(normalize-space(translate($sd, "-", ""))) &lt;= number(normalize-space(translate($ld_ancestor,"-",""))) ) and
( number(normalize-space(translate($sd, "-", "")))    >= number(normalize-space(translate($od_ancestor,"-",""))) )
)
or
( $dossierPerso = "Dossier de personnel" )
)
'>


<!--
<assert test='(

(
( number(normalize-space(translate($sd, "-", ""))) &lt;= number(normalize-space(translate($ld         ,"-",""))) ) and
( number(normalize-space(translate($sd, "-", "")))    >= number(normalize-space(translate($od         ,"-",""))) )
)
or
( $dossierPerso = "Dossier de personnel" )
)
'>
-->




+++
Erreur : Appraisal StartDate = <value-of select="$sd"  /> n'est pas comprise entre la OldestDate = <value-of select="$od"  /> <value-of select="$od_ancestor"  /> et la LatestDate = <value-of select="$ld"  /> <value-of select="$ld_ancestor"  /> applicables au bloc "<value-of select="./seda:Name"/>". Une StartDate n'est pas autorisée en dehors de cet intervalle sauf pour la typologie "Dossier de personnel".
+++
</assert>

</rule>

</pattern>




<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle B4: Appraisal: date application du sort final d'un objet doit être antérieure au parent

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->


<pattern>


<rule context="seda:ArchiveObject[ seda:AppraisalRule ]" >


<let name="courantStartDate" value="

./seda:AppraisalRule/seda:StartDate

" />


<let name="courantDuration" value="
translate(
translate( ./seda:AppraisalRule/seda:Duration , 'P' , '' ),
'Y' , '' )
" />


<let name="ancestorStartDate" value="

ancestor::seda:*[seda:AppraisalRule]/seda:AppraisalRule/seda:StartDate

" />


<let name="ancestorDuration" value="

translate(
translate( ancestor::seda:*[seda:AppraisalRule]/seda:AppraisalRule/seda:Duration , 'P' , '' ),
'Y' , '' )

" />


<assert test=" 

number(
normalize-space(translate(
$ancestorStartDate
, '- ', ''))
+ $ancestorDuration*10000)

>=

number(
normalize-space(translate(
$courantStartDate
, '- ', ''))
+ $courantDuration*10000) "

>
+++
Erreur: la date d'application du sort final <value-of select="./seda:Name"/> est plus lointaine que celle de son objet parent.
- fils  : <value-of select="$courantStartDate"  /> + <value-of select="$courantDuration"  /> ans
- parent: <value-of select="$ancestorStartDate"  /> + <value-of select="$ancestorDuration"  /> ans
+++
</assert>


</rule>

</pattern>



<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle C1: AccessRestriction, contrôle de la valeur de StartDate avec LatestDate de l'objet

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>

<!--
<rule context="seda:ArchiveObject[ seda:AccessRestrictionRule/seda:StartDate ]">
-->
<rule context="seda:*[ seda:AccessRestrictionRule/seda:StartDate ]">



<let name="sd"
value="seda:AccessRestrictionRule/seda:StartDate"
/>

<let name="except"
value="count( ./seda:AccessRestrictionRule/seda:Code[
( . = 'AR043' ) or
( . = 'AR044' ) or
( . = 'AR045' ) or
( . = 'AR046' ) or
( . = 'AR047' ) or
( . = 'AR061' ) 
] )"
/>

<let name="ld"
value="seda:ContentDescription/seda:LatestDate"
/>

<let name="od"
value="seda:ContentDescription/seda:OldestDate"
/>

<let name="ld_ancestor"
value="ancestor::seda:*/seda:ContentDescription/seda:LatestDate"
/>

<let name="od_ancestor"
value="ancestor::seda:*/seda:ContentDescription/seda:OldestDate"
/>



<!--
<let name="ld_child"
value="child::seda:*/seda:ContentDescription/seda:LatestDate"
/>
-->

<!--
<assert test="( ( $sd = $ld ) or ( $sd = $ld_ancestor ) or ( $sd = $ld_child ) ) or ( $except > 0 )" >
-->


<!-- PDE MODIFICATION, 20170331 -->
<!-- Note: le caractère < est interdit dans une assertion, alors il est remplacé par &lt; -->

<!--
<assert test='( ( number(translate($sd, "-", "")) &lt;= number(translate($ld, "-", "")) )
or ( number(translate($sd, "-", "")) &lt;= number(translate($ld_ancestor, "-", "")) )
or ( $except > 0 )
 )'>
-->

<!--
<assert test='(
(
( number(normalize-space(translate($sd, "-", ""))) &lt;= number(normalize-space(translate($ld         ,"-",""))) ) and
( number(normalize-space(translate($sd, "-", "")))    >= number(normalize-space(translate($od         ,"-",""))) )
)
or
(
( number(normalize-space(translate($sd, "-", ""))) &lt;= number(normalize-space(translate($ld_ancestor,"-",""))) ) and
( number(normalize-space(translate($sd, "-", "")))    >= number(normalize-space(translate($od_ancestor,"-",""))) )
)
or
( $except > 0 )
)
'>
-->

<assert test='(
(
( number(normalize-space(translate($sd, "-", ""))) &lt;= number(normalize-space(translate($ld         ,"-",""))) ) and
( number(normalize-space(translate($sd, "-", "")))    >= number(normalize-space(translate($od         ,"-",""))) )
)
or
( $except > 0 )
)
'>








<!--
<assert test="( ( $sd = $ld ) or ( $sd = $ld_ancestor ) ) or ( $except > 0 )" >
-->
+++
Erreur: AccessRestriction StartDate = <value-of select="$sd"  /> n'est pas comprise entre la
OldestDate = <value-of select="$od"  />  et la LatestDate = <value-of select="$ld"  />  applicables
au bloc <value-of select="./seda:Name"/>.
Une StartDate n'est pas autorisée en dehors de cet intervalle hormis pour les codes: AR043, AR044, AR045, AR046, AR047, AR061.
+++






</assert>

</rule>

</pattern>








<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle C2: AccessRestriction, contrôle de cohérence des dates de communicabilité

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->


<pattern>

<!--
<rule context="seda:*[ seda:AccessRestrictionRule/seda:StartDate ]">
-->

<rule context="seda:ArchiveObject[ seda:AccessRestrictionRule/seda:StartDate ]" >


<let name="courantStartDate" value="
./seda:AccessRestrictionRule/seda:StartDate
" />


<let name="courantCode" value="

25*count(
./seda:AccessRestrictionRule/seda:Code[ (.='AR039') or (.='AR040') or (.='AR041') or (.='AR042') ]
) +
25*count(
./seda:AccessRestrictionRule/seda:Code[ (.='AR043') or (.='AR044') or (.='AR045') or (.='AR046')  or (.='AR047') ]
) +
50*count(
./seda:AccessRestrictionRule/seda:Code[ (.='AR048') or (.='AR049') or (.='AR050') or (.='AR051') ]
) +
75*count(
./seda:AccessRestrictionRule/seda:Code[ (.='AR052') or (.='AR053') or (.='AR054') or (.='AR055') or (.='AR056') or (.='AR057') ]
) +
100*count(
./seda:AccessRestrictionRule/seda:Code[ (.='AR058') or (.='AR059') or (.='AR060') ]
) +
120*count(
./seda:AccessRestrictionRule/seda:Code[ (.='AR061') ]
)

" />

<!--
<let name="ancestorStartDate" value="
ancestor::seda:*[seda:AccessRestrictionRule]/seda:AccessRestrictionRule/seda:StartDate
" />
-->

<let name="ancestorStartDate" value="
ancestor::seda:*/seda:AccessRestrictionRule/seda:StartDate
" />

<let name="ancestorCode"
value="
25*count(
ancestor::seda:*[seda:AccessRestrictionRule/seda:Code[ (.='AR039') or (.='AR040') or (.='AR041') or (.='AR042') ] ]
) +
25*count(
ancestor::seda:*[seda:AccessRestrictionRule/seda:Code[ (.='AR043') or (.='AR044') or (.='AR045') or(.='AR046')or(.='AR047') ] ]
) +
50*count(
ancestor::seda:*[seda:AccessRestrictionRule/seda:Code[ (.='AR048') or (.='AR049') or (.='AR050') or (.='AR051') ] ]
) +
75*count(
ancestor::seda:*[seda:AccessRestrictionRule/seda:Code[ (.='AR052') or (.='AR053') or (.='AR054') or (.='AR055') or (.='AR056') or (.='AR057') ] ]
) +
100*count(
ancestor::seda:*[seda:AccessRestrictionRule/seda:Code[ (.='AR058') or (.='AR059') or (.='AR060') ] ]
) +
120*count(
ancestor::seda:*[seda:AccessRestrictionRule/seda:Code[ (.='AR062') ] ]
)
" />



<assert test=" 

number(
normalize-space(translate(
$ancestorStartDate
, '- ', ''))
+ $ancestorCode*10000)

>=

number(
normalize-space(translate(
$courantStartDate
, '- ', ''))
+ $courantCode*10000) "

>

+++
Erreur: la date de communicabilité de l'objet <value-of select="./seda:Name"/> est plus lointaine que celle de son objet parent.
- fils  : <value-of select="$courantStartDate"  /> + <value-of select="$courantCode"  /> ans
- parent: <value-of select="$ancestorStartDate"  /> + <value-of select="$ancestorCode"  /> ans
+++
</assert>


</rule>
</pattern>


<!--
====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+

	Règle D1: AccessRestriction StartDate = Appraisal StartDate

====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+====-====+
-->

<pattern>
<rule context="seda:*[ (seda:AccessRestrictionRule/seda:StartDate) and (seda:AppraisalRule/seda:StartDate) ]">

<let name="dossierPerso"
value="seda:ContentDescription/seda:Keyword/seda:KeywordContent"
/>

<let name="sda"
value="seda:AccessRestrictionRule/seda:StartDate"
/>
<let name="sdd"
value="seda:AppraisalRule/seda:StartDate"
/>

<let name="except"
value="count( ./seda:AccessRestrictionRule/seda:Code[
( . = 'AR043' ) or
( . = 'AR044' ) or
( . = 'AR045' ) or
( . = 'AR046' ) or
( . = 'AR047' ) or
( . = 'AR061' ) 
] )"
/>


<assert test='( ( $sda = $sdd )  ) or ( $except > 0 ) or ( $dossierPerso = "Dossier de personnel" )' >
+++
Erreur: AccessRestriction StartDate = <value-of select="$sda" />
n'est pas égale à Appraisal StartDate = <value-of select="$sdd"  />
pour un même niveau de description.
Une différence entre ces deux dates n'est autorisée que pour la typologie 'Dossier de personnel'
ou lorsque les codes de communicabilité sont AR043, AR044, AR045, AR046, AR047, AR061.
+++
</assert>

</rule>
</pattern>

</schema>
