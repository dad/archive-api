/*
 * Copyright CINES, 2017 Ce logiciel est un programme informatique servant à créer une interface Web
 * pour valider des formats de fichiers. Ce logiciel est régi par la licence CeCILL-C soumise au
 * droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 * redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 * l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 * à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 * leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 * leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 * sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */
package fr.cines.pac.service.ingest.xml;

/**
 * La classe SedaConstants.
 */
public final class SedaConstants {

    /** La constante ACCESS_RESTRICTION_CODE_XSD_FILENAME. */
    public static final String ACCESS_RESTRICTION_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_accessrestriction_code.xsd";

    /** La constante LANGUAGE_CODE_XSD_FILENAME. */
    public static final String LANGUAGE_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_language_code.xsd";

    /** La constante ORGANIZATION_XSD_FILENAME. */
    public static final String ORGANIZATION_XSD_FILENAME = "seda/seda_v1-0_organization.xsd";

    /** La constante OTHER_METADATA_XSD_FILENAME. */
    public static final String OTHER_METADATA_XSD_FILENAME = "seda/othermetadata.xsd";

    /** La constante IANA_CHARACTER_SET_CODE_XSD_FILENAME. */
    public static final String IANA_CHARACTER_SET_CODE_XSD_FILENAME = "seda/codes/IANA_CharacterSetCode_20101104.xsd";

    /** La constante UNECE_CHARACTER_SET_ENCODING_CODE_XSD_FILENAME. */
    public static final String UNECE_CHARACTER_SET_ENCODING_CODE_XSD_FILENAME = "seda/codes/UNECE_CharacterSetEncodingCode_40106.xsd";

    /** La constante IANA_MIME_MEDIA_TYPE_XSD_FILENAME. */
    public static final String IANA_MIME_MEDIA_TYPE_XSD_FILENAME = "seda/codes/IANA_MIMEMediaType_20110216.xsd";

    /** La constante ISO_ISO2L_COUNTRY_CODE_XSD_FILENAME. */
    public static final String ISO_ISO2L_COUNTRY_CODE_XSD_FILENAME = "seda/codes/ISO_ISOTwoletterCountryCode_SecondEdition2006VI-8.xsd";
    
    /** La constante ISO_ISO3L_COUNTRY_CODE_XSD_FILENAME. */
    public static final String ISO_ISO3L_COUNTRY_CODE_XSD_FILENAME = "seda/codes/ISO_ISO3AlphaCurrencyCode_20110218.xsd";

    /** La constante UNECE_COMMUNICATION_MEANS_TYPE_CODE_XSD_FILENAME. */
    public static final String UNECE_COMMUNICATION_MEANS_TYPE_CODE_XSD_FILENAME = "seda/codes/UNECE_CommunicationMeansTypeCode_D10A.xsd";

    /** La constante APPRAISAL_CODE_XSD_FILENAME. */
    public static final String APPRAISAL_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_appraisal_code.xsd";

    /** La constante DESCRIPTION_LEVEL_CODE_XSD_FILENAME. */
    public static final String DESCRIPTION_LEVEL_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_descriptionlevel_code.xsd";

    /** La constante KEYWORD_TYPE_CODE_XSD_FILENAME. */
    public static final String KEYWORD_TYPE_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_keywordtype_code.xsd";

    /** La constante DOCUMENT_TYPE_CODE_XSD_FILENAME. */
    public static final String DOCUMENT_TYPE_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_documenttype_code.xsd";

    /** La constante FILE_TYPE_CODE_XSD_FILENAME. */
    public static final String FILE_TYPE_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_filetype_code.xsd";

    /** La constante UNIT_IDENTIFIER_CODE_XSD_FILENAME. */
    public static final String UNIT_IDENTIFIER_CODE_XSD_FILENAME = "seda/codes/seda_v1-0_unitidentifier_code.xsd";
    
    /** La constante AGENCY_IDENTIFICATION_CODE_XSD_FILENAME. */
    public static final String AGENCY_IDENTIFICATION_CODE_XSD_FILENAME = "UNECE_AgencyIdentificationCode_D10A.xsd";
    
    /** La constante AUTHORISATION_REASON_CODE_XSD_FILENAME. */
    public static final String AUTHORISATION_REASON_CODE_XSD_FILENAME = "seda_v1-0_authorisationReason_code.xsd";
    
    /** La constante MEASUREMENT_UNIT_CODE_XSD_FILENAME. */
    public static final String MEASUREMENT_UNIT_CODE_XSD_FILENAME = "UNECE_MeasurementUnitCommonCode_7.xsd";

    /** La constante SEDA1_ARCHIVE_TRANSFER_FILENAME. */
    public static final String SEDA1_ARCHIVE_TRANSFER_FILENAME = "ArchiveTransfer.xml";
    
    /** La constante SEDA1_ARCHIVE_FILENAME. */
    public static final String SEDA1_ARCHIVE_FILENAME = "Archive.xml";

    /** La constante SEDA1_ARCHIVE_DELIVERY_REQUEST_FILENAME. */
    public static final String SEDA1_ARCHIVE_DELIVERY_REQUEST_FILENAME = "ArchiveDeliveryRequest.xml";
    
    /** La constante SEDA1_ARCHIVE_DELIVERY_REQUEST_REPLY_FILENAME. */
    public static final String SEDA1_ARCHIVE_DELIVERY_REQUEST_REPLY_FILENAME = "ArchiveDeliveryRequestReply.xml";
    
    /** La constante ARCHIVE_DELIVERY_REQUEST_XSD_FILENAME. */
    public static final String ARCHIVE_DELIVERY_REQUEST_XSD_FILENAME = "seda/seda_v1-0_archivedelivery.xsd";
    
    /** La constante ARCHIVE_TAG. */
    public static final String ARCHIVE_TAG = "Archive";

    /** La constante NAME_TAG. */
    public static final String NAME_TAG = "Name";

    /** La constante SEDA1_ARCHIVE_TRANSFER_XSD_FILENAME. */
    public static final String SEDA1_ARCHIVE_TRANSFER_XSD_FILENAME = "seda/seda_v1-0_archivetransfer.xsd";
    
    /** La constante SEDA1_ACKNOWLEDGEMENT_XSD_FILENAME. */
    public static final String SEDA1_ACKNOWLEDGEMENT_XSD_FILENAME = "seda/seda_v1-0_acknowledgement.xsd";
        
    /** La constante SEDA1_MIME_TYPE. */
    public static final String SEDA1_MIME_TYPE = "text/xml";

    /** La constante SEDA1_XMLNS. */
    public static final String SEDA1_XMLNS = "${seda1.xmlns}";

    /** La constante SEDA1_ARCHIVE_TRANSFER_XSD_LOCATION. */
    public static final String SEDA1_ARCHIVE_TRANSFER_XSD_LOCATION = "${seda1.xsd.location}";
    
    /** La constante SEDA1_ARCHIVE_DELIVERY_REQUEST_XSD_LOCATION. */
    public static final String SEDA1_ARCHIVE_DELIVERY_REQUEST_XSD_LOCATION = "${seda1.delivery.xsd.location}";
    
    /** La constante SEDA1_ACKNOWLEDGEMENT_XSD_LOCATION. */
    public static final String SEDA1_ACKNOWLEDGEMENT_XSD_LOCATION = "${seda1.ack.xsd.location}";

    /** La constante SEDA1_CINES_ARCHIVING_PROFILE. */
    public static final String SEDA1_CINES_ARCHIVING_PROFILE = "PA/PA_CINES.rng";

    /** La constante SEDA1_SCHEMATRON. */
    public static final String SEDA1_SCHEMATRON = "schematron/seda.sch";
    
    /** La constante TRANSFER_IDENTIFIER_TAG. */
    public static final String TRANSFER_IDENTIFIER_TAG = "TransferIdentifier";
    
    /** La constante DOCUMENT_TAG. */
    public static final String DOCUMENT_TAG = "Document";
    
    /** La constante ATTACHMENT_TAG. */
    public static final String ATTACHMENT_TAG = "Attachment";
    
    /** La constante SIZE_TAG. */
    public static final String SIZE_TAG = "Size";
    
    /** La constante INTEGRITY_TAG. */
    public static final String INTEGRITY_TAG = "Integrity";
    
    /** La constante FILENAME_ATTR. */
    public static final String FILENAME_ATTR = "filename";
    
    /** La constante FORMAT_ATTR. */
    public static final String FORMAT_ATTR = "format";
    
    /** La constante ALGORITHME_ATTR. */
    public static final String ALGORITHME_ATTR = "algorithme";
    
    /** La constante UNIT_CODE_ATTR. */
    public static final String UNIT_CODE_ATTR = "unitCode";
    
    /** La constante CREATION_TAG. */
    public static final String CREATION_TAG = "Creation";
    
    /** La constante BYTES_UNIT_CODE. */
    public static final String BYTES_UNIT_CODE = "AD";
    
    /** La constante XML_FORMAT. */
    public static final String XML_FORMAT = "fmt/101";
    
    /** La constante SEDA1_SCHEMATRON_XSL_ROOT. */
    public static final String SEDA1_SCHEMATRON_XSL_ROOT = "schematron/iso-schematron-xslt2";
    
    /** La constante SEDA1_SCHEMATRON_INCLUDE. */
    public static final String SEDA1_SCHEMATRON_INCLUDE = SEDA1_SCHEMATRON_XSL_ROOT + "/iso_dsdl_include.xsl";
    
    /** La constante SEDA1_SCHEMATRON_EXPAND. */
    public static final String SEDA1_SCHEMATRON_EXPAND = SEDA1_SCHEMATRON_XSL_ROOT + "/iso_abstract_expand.xsl";
    
    /** La constante SEDA1_SCHEMATRON_SVRL. */
    public static final String SEDA1_SCHEMATRON_SVRL = SEDA1_SCHEMATRON_XSL_ROOT + "/iso_svrl_for_xslt2.xsl";

    /**
     * Instanciation privée.
     */
    private SedaConstants() { // NOSONAR
        super();
    }
}
