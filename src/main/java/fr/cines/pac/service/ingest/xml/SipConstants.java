/*
 * Copyright CINES, 2017 Ce logiciel est un programme informatique servant à créer une interface Web
 * pour valider des formats de fichiers. Ce logiciel est régi par la licence CeCILL-C soumise au
 * droit français et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffusée par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilité au code source et des droits de copie, de modification et de
 * redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les concédants successifs. A cet égard
 * l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation,
 * à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à
 * leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de
 * leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de
 * sécurité. Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accepté les termes.
 */
package fr.cines.pac.service.ingest.xml;

/**
 * La classe SipConstants.
 */
public final class SipConstants {

    /** La constante ACCESS_RESTRICTION_CODE_XSD_FILENAME. */
    public static final String ACCESS_RESTRICTION_CODE_XSD_FILENAME = SedaConstants.ACCESS_RESTRICTION_CODE_XSD_FILENAME;

    /** La constante DATE_NOT_SPECIFIED. */
    public static final String DATE_NOT_SPECIFIED = "s.d.";

    /** La constante DATE_TAG. */
    public static final String DATE_TAG = "date";

    /** La constant DEFAULT_CHECKSUM_TYPE. */
    public static final String DEFAULT_CHECKSUM_TYPE = "SHA-256";

    /** La constante DIFFUSION_CONTAINER décrivant le données liées à la diffusion. */
    public static final String DIFFUSION_CONTAINER = "DIFFUSION";

    /** La constante DOC_DC_TAG. */
    public static final String DOC_DC_TAG = "DocDC";

    /** La constante DOC_META_TAG. */
    public static final String DOC_META_TAG = "DocMeta";

    /** La constante FILE_META_TAG. */
    public static final String FILE_META_TAG = "FichMeta";

    /** La constante FILENAME_TAG. */
    public static final String FILENAME_TAG = "nomFichier";

    /** La constante LANGUAGE_CODE_XSD_FILENAME. */
    public static final String LANGUAGE_CODE_XSD_FILENAME = SedaConstants.LANGUAGE_CODE_XSD_FILENAME;

    /** La constante ORIGINAL_CHECKSUM_TAG. */
    public static final String ORIGINAL_CHECKSUM_TAG = "empreinteOri";

    /** La constante ORIGINAL_CHECKSUM_TYPE_ATTR. */
    public static final String ORIGINAL_CHECKSUM_TYPE_ATTR = "type";

    /** La constante PAC_DOCUMENT_UPDATE_ID_TAG. */
    public static final String PAC_DOCUMENT_UPDATE_ID_TAG = "identifiantDocMajPac";

    /** La constante PAC_PREVIOUS_VERSION_ID_TAG. */
    public static final String PAC_PREVIOUS_VERSION_ID_TAG = "idVersionPrecedentePac";

    /** La constante PRODUCER_DOCUMENT_UPDATE_ID_TAG. */
    public static final String PRODUCER_DOCUMENT_UPDATE_ID_TAG = "identifiantDocMajProducteur";

    /** La constante PRODUCER_PREVIOUS_VERSION_ID_TAG. */
    public static final String PRODUCER_PREVIOUS_VERSION_ID_TAG = "idVersionPrecedenteProducteur";

    /** La constante RELATION_SOURCE_PAC. */
    public static final String RELATION_SOURCE_PAC = "PAC";

    /** La constante RELATION_SOURCE_PRODUCTEUR. */
    public static final String RELATION_SOURCE_PRODUCTEUR = "Producteur";

    /** La constante RELATION_TAG. */
    public static final String RELATION_TAG = "docRelation";

    /** La constante RELATION_TYPE_FILIATION. */
    public static final String RELATION_TYPE_FILIATION = "filiation";

    /** La constante RELATION_TYPE_TAG. */
    public static final String RELATION_TYPE_TAG = "typeRelation";

    /** La constante RELATION_TYPE_UPDATE. */
    public static final String RELATION_TYPE_UPDATE = "maj";

    /** La constante RELATION_TYPE_VERSION. */
    public static final String RELATION_TYPE_VERSION = "version";

    /** La constante SIP_FILENAME. */
    public static final String SIP_FILENAME = "sip.xml";

    /** La constante SIP_MIME_TYPE. */
    public static final String SIP_MIME_TYPE = "text/xml";

    /**
     * La constante FILES_CONTAINER décrivant le répertoire 'DEPOT' ou sont déposés les fichiers autres que
     * les bordereaux.
     */
    public static final String FILES_CONTAINER = "DEPOT";

    /** La constante IP_XSD_FILENAME. */
    public static final String IP_XSD_FILENAME = "ip.xsd";

    /** La constante SIP_XSD_FILENAME. */
    public static final String SIP_XSD_FILENAME = "sip.xsd";

    /** La constante VALUE_NOT_SPECIFIED. */
    public static final String VALUE_NOT_SPECIFIED = "Non renseigné";

    /** La constante SIP_XMLNS. */
    public static final String SIP_XMLNS = "${sip.xmlns}";

    /** La constante SIP_XSD_LOCATION. */
    public static final String SIP_XSD_LOCATION = "${sip.xsd.location}";

    /** La constante METADATA_CONTAINER décrit le répertoire 'DESC' où sont déposées les métadonnées. */
    public static final String METADATA_CONTAINER = "DESC";

    /** La constante WORKFLOW_TAG. */
    public static final String WORKFLOW_TAG = "workflow";

    /** La constante SERVICE_TAG. */
    public static final String SERVICE_TAG = "service";

    /** La constante ORDER_ATTR. */
    public static final String ORDER_ATTR = "order";

    /** La constante PARAM_ATTR. */
    public static final String PARAM_ATTR = "param";

    /** La constante SKIP_ATTR. */
    public static final String SKIP_ATTR = "skip";

    /** La constante AIP_FILENAME. */
    public static final String AIP_FILENAME = "aip.xml";

    /** La constante AIP_MIME_TYPE. */
    public static final String AIP_MIME_TYPE = "application/xml";

    /** La constante AIP_XSD_FILENAME. */
    public static final String AIP_XSD_FILENAME = "aip.xsd";

    /** La constante UNDEFINED. */
    public static final String UNDEFINED = "NA";

    /** La constante AIP_XMLNS. */
    public static final String AIP_XMLNS = "${aip.xmlns}";

    /** La constante XSD_LOCATION. */
    public static final String AIP_XSD_LOCATION = "${aip.xsd.location}";

    /**
     * Instanciation privée.
     */
    private SipConstants() { // NOSONAR
        super();
    }
}
