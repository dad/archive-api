# archive-api

Librairie de mapping des schémas SEDA 1.0, SEDA 2.1 et CINES Dublin Core.
Initialement inclue dans le module d'entrée distribué de PACV3.

# Listes des modifications apportées par rapport au SEDA 2.1

## Ajout de l'ontologie issue du Dublin Core
Au niveau de la balise "content"

## Ajout des modifications SEDA Vitam 
Au niveau de la balise "management", permettre l'ajout d'une balise "updateOperation".
